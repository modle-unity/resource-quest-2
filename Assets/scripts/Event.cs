using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Event {
    public string name;
    public string[] description;
    public string type;
    public string effect;
    public string target;

    public bool active = false;
    public float startTime;
    public float endTime;
    public int lastTriggered = 0;
    public float triggerChance;
    int direction;

    public float duration = 0f;
    int minDuration = 25;
    int maxDuration = 30;

    public int potency;
    int minPotency = 3;
    int maxPotency = 8;

    public Event(string _name, string[] _description, string _type, string _effect, string _target, int _direction) {
        name = _name;
        description = _description;
        type = _type;
        effect = _effect;
        target = _target;
        direction = _direction;
        // SetDefaults();
    }

    void SetDefaults() {
        SetPotency();
        SetDuration();
        triggerChance = UnityEngine.Random.Range(0.0f, 0.05f);
    }

    private void SetPotency() {
        potency = direction * UnityEngine.Random.Range(minPotency, maxPotency);
    }

    private void SetDuration() {
        duration = UnityEngine.Random.Range(minDuration, maxDuration);
    }

    public void Trigger() {
        SetDuration();
        SetPotency();
        active = true;
        startTime = Utils.utils.currentGameTime;
        endTime = startTime + duration;
        Log.log.Add(string.Format("{0:s}\n", string.Join("\n", description)));
    }

    public void Update() {
        if (!active) {
            return;
        }
        // subtract time since frame, scaled with slider
        duration -= Time.deltaTime * SpeedSlider.slider.currentSliderValue;
        if (duration < 0) {
            Deactivate();
        }
    }

    public void Deactivate() {
        active = false;
    }

    public string DebugRepr() {
        string state = active ? "ACTIVE" : "DISABLED";
        return string.Format(
            "{0:d} | {1:s} | {2:s} | {3:s} | start: {4:f} | dur: {5:f} | end: {6:f} | {7:s}",
            potency, type, effect, name, startTime, duration, endTime, state
        );
    }

    public string Repr() {
        if (type == "production") {
            return string.Format("{0:s}\n{1:d} {2:s} halted", name, effect, type);
        } else {
            return string.Format("{0:s}\n{1:s}", name, effect);
        }
    }
}
