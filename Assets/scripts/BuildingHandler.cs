using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingHandler : MonoBehaviour {

    public static BuildingHandler handler;

    public Dictionary<string, Building> buildings;

    private Dictionary<string, int> posParams = new Dictionary<string, int>();
    private float xUnit;
    private float yUnit;

    void Awake() {
        // singleton pattern
        if (handler == null) {
            DontDestroyOnLoad(gameObject);
            handler = this;
        } else if (handler != this) {
            Destroy(gameObject);
        }
    }

    public void SetDefaults() {
        SetPositions();
        GenerateBuildings();
        UpdatePositions(posParams);
    }

    private void SetPositions() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        SetDefaultPositionParams();
    }

    private void SetDefaultPositionParams() {
        posParams["theMod"] = (int)(xUnit * 80);
        posParams["progressX"] = (int)(xUnit * 12);
        posParams["progressY"] = (int)(yUnit * 15);
        posParams["progressWidth"] = (int)(xUnit * 80);
        posParams["progressHeight"] = (int)(yUnit * 10);
        posParams["progressOffset"] = (int)(yUnit * 15f);
        posParams["labelX"] = (int)(xUnit * 12);
    }

    private void GenerateBuildings() {
        buildings = new Dictionary<string, Building>();
        buildings.Add(
            "forester",
            new Building(
                "forester", "Forester", "harvests wood",
                new Dictionary<string, int>() {},
                new Dictionary<string, int>() {{"wood", 3}, {"bow", 1}},
                buildings.Count, posParams)
            );
        buildings.Add(
            "gatherer",
            new Building(
                "gatherer", "Gatherer", "forages for food",
                new Dictionary<string, int>() {},
                new Dictionary<string, int>() {{"food", 4}},
                buildings.Count, posParams)
            );
        buildings.Add(
            "hunter",
            new Building(
                "hunter", "Hunter", "kills to live",
                new Dictionary<string, int>() {{"bow", 1}},
                new Dictionary<string, int>() {{"food", 2}, {"hide", 1}},
                buildings.Count, posParams)
            );
        buildings.Add(
            "mine",
            new Building(
                "mine", "Miner", "there's iron in them rocks",
                new Dictionary<string, int>() {},
                new Dictionary<string, int>() {{"iron", 2}},
                buildings.Count, posParams)
            );
        buildings.Add(
            "smithy",
            new Building(
                "smithy", "Smith", "tools to make life easier",
                new Dictionary<string, int>() {{"wood", 1}, {"iron", 1}},
                new Dictionary<string, int>() {{"tool", 1}, {"flint", 1}},
                buildings.Count, posParams)
            );
        buildings.Add(
            "woodcutter",
            new Building(
                "woodcutter", "Woodcutter", "firewood for warmth",
                new Dictionary<string, int>() {{"wood", 1}},
                new Dictionary<string, int>() {{"firewood", 5}},
                buildings.Count, posParams)
            );
        buildings.Add(
            "tailor",
            new Building(
                "tailor", "Tailor", "clothes to stave off cold",
                new Dictionary<string, int>() {{"hide", 2}},
                new Dictionary<string, int>() {{"clothing", 1}, {"pants", 1}},
                buildings.Count, posParams
            )
        );
    }

    public void Manage() {
        SetPositions();
        HandleClicks();
        UpdatePositions(posParams);
        UpdateProgress();
    }

    private void HandleClicks() {
        Vector2 touchPosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        foreach (KeyValuePair<string, Building> entry in buildings) {
            if (entry.Value.buildingRects.label.Contains(touchPosition)) {
                if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) {
                    Debug.Log("Building " + entry.Value.name + " clicked at " + Input.mousePosition);
                }
            }
        }
    }

    public void UpdatePositions(Dictionary<string, int> posParams) {
        foreach (KeyValuePair<string, Building> entry in buildings) {
            entry.Value.UpdatePosition(posParams);
        }
    }

    public void UpdateProgress() {
        CalculateMultipliers();
        foreach (KeyValuePair<string, Building> entry in buildings) {
            if (entry.Value.villagers <= 0 || State.state.paused) {
               continue;
            }
            entry.Value.ManageProductionState();
        }
    }

    private void CalculateMultipliers() {
        foreach (KeyValuePair<string, Building> entry in buildings) {
            entry.Value.villagersWithTools = VillagerHandler.handler.CountVillagersWithTools(entry.Value.name);
            entry.Value.villagers = VillagerHandler.handler.CountBuildingVillagers(entry.Value.name);
        }
    }

    public void RemoveVillager(string buildingName) {
        if (buildingName == "" || buildingName == "dead") {
            return;
        }
        Building building = buildings[buildingName];
        VillagerHandler.handler.UnassignVillagers(buildingName, 1);
    }

    public void Draw() {
        foreach (KeyValuePair<string, Building> entry in buildings) {
            entry.Value.Draw();
        }
    }

    public void Reset() {
        SetDefaults();
    }

    public string GetBuildingsDebugRepr() {
        string theString = "BUILDINGS";
        foreach (KeyValuePair<string, Building> entry in buildings) {
            theString += string.Format("\n{0:s}", entry.Value.DebugRepr());
        }
        return theString;
    }
}
