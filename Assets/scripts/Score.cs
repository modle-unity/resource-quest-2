using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public static Score score;

    public int value = 0;
    public int totalEarned = 0;
    public int tickValue = 1;
    public static Rect textBox = Rect.zero;
    public static Rect spriteRect = Rect.zero;
    public static CustomFont font;
    private int fontScale = 4;
    public List<Texture2D> textures = new List<Texture2D>();
    private static float xUnit;
    private static float yUnit;
    private bool added;
    public int spriteTextureKey = 0;

    void Awake() {
        // singleton pattern
        if (score == null) {
            DontDestroyOnLoad(gameObject);
            score = this;
        } else if (score != this) {
            Destroy(gameObject);
        }
        LoadTextures();
        SetDefaults();
    }

    public void LoadTextures() {
        textures.Add((Texture2D)Resources.Load("coin/coin0"));
        textures.Add((Texture2D)Resources.Load("coin/coin1"));
        textures.Add((Texture2D)Resources.Load("coin/coin2"));
        textures.Add((Texture2D)Resources.Load("coin/coin3"));
    }

    public void SetDefaults() {
        // do not see how this was getting called before this change to call during Awake
        // what was setting position?
        font = new CustomFont(Color.black, fontScale);
    }

    public void Update() {
        UpdatePositionValues();
        if (State.state.CheckTick() && State.state.IsActive()) {
            if (added) {
                return;
            }
            Add(tickValue);
            return;
        }
        added = false;
    }

    public static void UpdatePositionValues() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        textBox.Set(xUnit * 10, yUnit * 3, 1, 1);
        spriteRect.Set(xUnit * 5, yUnit * 3, xUnit * 2, yUnit * 3);
        font.Update();
    }

    public void Add(int adjustValue) {
        if (!State.state.gameOver) {
            value += adjustValue;
            totalEarned += adjustValue;
            added = true;
        }
    }

    public bool Remove(int adjustValue) {
        if (!State.state.gameOver) {
            if (value - adjustValue >= 0) {
                value -= adjustValue;
                return true;
            }
        }
        return false;
    }

    public void Draw() {
        font.font.normal.textColor = Utils.utils.textColor;
        if (!State.state.showHelp) {
            GUI.Label(textBox, GetText(), font.font);
            GUI.DrawTexture(spriteRect, GetCurrentTexture());
        }
    }

    public Texture2D GetCurrentTexture() {
        // TODO this will do for now, but it returns a sequence like 331133000011111122223333003311222
        spriteTextureKey = Utils.utils.OscillateSprite(textures, 10);
        return textures[spriteTextureKey];
    }

    private string GetText() {
        return Utils.utils.SquishNumber(value);
    }

    public void Reset() {
        SetDefaults();
        value = 0;
        font.Update();
    }
}
