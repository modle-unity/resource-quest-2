using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

[Serializable]
public class Stat {
    public string name { get; set; }
    public int minValue { get; set; }
    public int maxValue { get; set; }
    public int value { get; set; }
    public List<string> consumes;
    public string condition { get; set; }
    public int conditionTrigger = 0;
    public int consumeAmount = 1;
    public int consumeTrigger = 1;
    int defaultReplenishValue = 100;
    int replenishValue = 0;
    public int timeSinceExhausted = 0;

    public Stat(string _name, int _minValue, int _maxValue, string _consumes, string _condition) {
        name = _name;
        minValue = _minValue;
        maxValue = _maxValue;
        value = _maxValue;
        this.consumes = _consumes.Split(char.Parse(",")).ToList<string>();
        condition = _condition;
        replenishValue = (int)(defaultReplenishValue / consumes.Count) - 1;
    }

    public void AdjustValue(int amount) {
        if (amount > 0) {
            return;
        }
        this.value += (this.value > this.minValue ? amount : 0);
    }

    public void Replenish() {
        if (this.value < this.consumeTrigger) {
            bool addStat = false;
            foreach (string targetResource in this.consumes) {
                bool consumed = StoreHandler.handler.stores[targetResource].Consume(this.consumeAmount);
                if (consumed) {
                    this.value += this.replenishValue;
                    this.timeSinceExhausted = 0;
                }
                addStat = consumed || addStat;
            }
            if (!addStat) {
                this.timeSinceExhausted += 1;
            }
        }
    }
}
