using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour {

    public static State state;

    public bool gameOver;
    public bool paused;
    public bool restart;
    public string screen;
    public bool showHelp;
    public bool showSettings;
    public int toolPower = 2;

    int tick = 10;
    int timeMultiplier = 1;

    int fontScale = 5;

    void Awake() {
        // singleton pattern
        if (state == null) {
            DontDestroyOnLoad(gameObject);
            state = this;
        } else if (state != this) {
            Destroy(gameObject);
        }
        SetDefaults();
    }

    private void SetDefaults() {
        gameOver = false;
        paused = false;
        showHelp = false;
        showSettings = false;
        restart = false;
        screen = "main";
    }

    public bool CheckTick() {
        if (!VillagerHandler.handler.active || paused) {
            return false;
        }

        // given time of 3 seconds since start
        // 1; 10 - 1 * 10/5 + 10/5 = 10 // every 10 seconds
        //      10 % 10 == 0;  true
        // 2; 10 - 2 * 10/5 + 10/5 = 8 // every 8 seconds
        //      10 % 24 == 6;  false
        // 3; 10 - 3 * 10/5 + 10/5 = 6 // every 6 seconds
        //      10 % 18 == 12; false
        // 4; 10 - 4 * 10/5 + 10/5 = 4 // every 4 seconds
        //      10 % 12 == 6;  false
        // 5; 10 - 5 * 10/5 + 10/5 = 2 // every 2 seconds
        //      10 % 6  == 0;  true
        // why / 5, scale?
        int tickCheck = (int)(tick - SpeedSlider.slider.currentSliderValue * tick / 5 + tick / 5);

        // Time.time is the seconds since the start of the application
        // this is set at the start of each frame
        int currentTime = (int)(Time.time * timeMultiplier);
        bool tickIsEligible = (currentTime % tickCheck == 0);
        return tickIsEligible;
    }

    public void HandleStates() {
        if (paused || showHelp || showSettings) {
            DrawOverlay();
        }
        if (showHelp) {
            DrawHelpMessage();
        } else if (paused) {
            DrawPausedMessage();
        }
        if (restart) {
            GameControl.control.Restart();
        }
    }

    public void DrawOverlay() {
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
    }

    private void DrawPausedMessage() {
        GUI.Label(
            new Rect(
                Screen.width / 4,
                Screen.height / 3,
                Screen.width / 10,
                Screen.height / 10
            ),
            "PAUSED",
            (new CustomFont(Utils.utils.textColor, fontScale)).font
        );
    }

    private void DrawHelpMessage() {
        // don't do anything here; each component will check showHelp,
        // if true, draw help messages
        GUI.Label(
            new Rect(
                Screen.width / 4,
                Screen.height / 3,
                Screen.width / 10,
                Screen.height / 10
            ),
            "",
            (new CustomFont(Utils.utils.textColor, fontScale)).font
        );
    }

    public void ResetScreens() {
        showHelp = false;
        showSettings = false;
        ButtonHandler.reloadClicked = false;
        screen = "main";
    }

    public void Reset() {
        SetDefaults();
    }

    public bool IsActive() {
        return !paused && !gameOver && VillagerHandler.handler.active;
    }

    public bool MouseDown() {
        return Input.GetMouseButton(0);
    }
}
