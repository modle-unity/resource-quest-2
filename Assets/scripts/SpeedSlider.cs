using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedSlider : MonoBehaviour {

    public static SpeedSlider slider;

    public Rect sliderRect = Rect.zero;
    public Rect trackRect = Rect.zero;
    public Rect fixedRect = Rect.zero;
    public Rect labelRect = Rect.zero;
    // sliderTexture is intentionally left null to get the default button texture
    public Texture2D sliderTexture;
    public Texture2D fixedTexture;
    public CustomFont font;
    private int fontScale = 5;

    public float speedSliderX;
    public float currentSliderValue;
    int speedScale = 25;
    float sliderLength;
    float maxSliderValue;

    private Rect posParams = Rect.zero;
    private float xUnit;
    private float yUnit;

    private int trackWidthMultiplier = 20;
    private int trackHeightMultiplier = 8;

    private int horizontalOffsetMultiplier = 20;
    private int verticalOffsetMultiplier = 4;

    public bool clickedOn = false;

    void Awake() {
        // singleton pattern
        if (slider == null) {
            DontDestroyOnLoad(gameObject);
            slider = this;
        } else if (slider != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        SetDefaults();
    }

    void SetDefaults() {
        font = new CustomFont(Color.white, fontScale);
        LoadTextures();
        SetDefaultPositionParams();
    }

    private void Update() {
        SetPositionValues();
        SetDefaultPositionParams();
        SetRectParameters();
        sliderLength = fixedRect.width - sliderRect.width;
        maxSliderValue = CalculateSliderValue(sliderLength);
        CheckSliderLimits();
        font.Update();
    }

    private void LoadTextures() {
        fixedTexture = (Texture2D)Resources.Load("fixed");
    }

    private void SetPositionValues() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
    }

    private void SetDefaultPositionParams() {
        posParams.Set(
            xUnit * horizontalOffsetMultiplier,
            yUnit * verticalOffsetMultiplier,
            xUnit * 3,
            yUnit * 0.75f
        );
    }

    private void SetRectParameters() {
        // the part that moves
        sliderRect.Set(
            speedSliderX > 0 ? speedSliderX : posParams.x,
            posParams.y,
            posParams.width * 3,
            posParams.height * trackHeightMultiplier
        );

        // the invisible full-width bar of the slider, used to hold position
        trackRect.Set(
            posParams.x,
            posParams.y,
            posParams.width * trackWidthMultiplier + sliderRect.width,
            posParams.height * trackHeightMultiplier
        );

        // the visible full-width bar of the slider, used to show varied visiualization of the slider
        fixedRect.Set(
            posParams.x,
            posParams.y,
            posParams.width * trackWidthMultiplier + sliderRect.width,
            posParams.height * trackHeightMultiplier
        );

        // container for the multipler number
        labelRect.Set(
            posParams.x,
            posParams.y,
            posParams.width * trackHeightMultiplier,
            posParams.height
        );
    }

    public void Draw() {
        if (!State.state.showHelp) {
            Drag();
            GUI.DrawTexture(this.fixedRect, this.fixedTexture);
            GUI.Button(this.sliderRect, this.sliderTexture);
            GetCurrentSliderMultiplier();
            // convert value to string with 2 decimal places
            GUI.Label(this.labelRect, "x" + currentSliderValue.ToString("F2"), this.font.font);
        }
    }

    private void GetCurrentSliderMultiplier() {
        speedSliderX = sliderRect.x;
        float currentPos = sliderRect.x - fixedRect.x;
        currentSliderValue = CalculateSliderValue(currentPos);
    }

    private float CalculateSliderValue(float position) {
        // scale to 1-100 range with: 1 + 100 * (x - min(x)) / (max(x) - min(x))
        return 1 + 100 * position / sliderLength / speedScale;
    }

    public void Drag() {
        // keep label position synced with slider position
        this.labelRect.x = sliderRect.x + sliderRect.width / 8;
        this.labelRect.y = sliderRect.y + sliderRect.height / 8;

        // capture mouse position
        Vector2 touchPosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        // toggles ability to move if mouse was clicked inside the slider container rect
        if (!clickedOn && Input.GetMouseButtonDown(0) && trackRect.Contains(touchPosition)) {
            clickedOn = true;
        }
        // disables ability to move if mouse was released
        if (clickedOn && Input.GetMouseButtonUp(0)) {
            clickedOn = false;
        }
        // if cannot move, do nothing and flag UI for unlock if locked by slider
        if (!clickedOn) {
            GameControl.control.UnlockUI("slider");
            return;
        }

        GameControl.control.LockUIByType("slider");

        // sync slider rect with touch/mouse position
        if (Input.GetMouseButton(0) && clickedOn) {
            sliderRect.x = touchPosition.x - sliderRect.width / 2;
        }

        CheckSliderLimits();
    }

    private void CheckSliderLimits() {
        // handle minimum position of slider (all the way left)
        if (sliderRect.x <= trackRect.x) {
            sliderRect.x = trackRect.x;
        }
        // handle maximum position of slider (all the way right)
        float maxPosition = trackRect.x + trackRect.width - sliderRect.width;
        if (sliderRect.x > maxPosition) {
            sliderRect.x = maxPosition;
        }
    }

    public void Reset() {
        SetDefaults();
    }
}
