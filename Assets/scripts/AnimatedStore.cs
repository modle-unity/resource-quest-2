using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedStore {
    public Rect defaultRect;
    public Rect addLabelRect;
    public Rect subtractLabelRect;
    public Texture2D texture;
    private int quantity;
    public float aliveTime;
    public bool drawMe;
    public float maxAliveTime = 0.75f;
    public Dictionary<string, CustomFont> fonts = new Dictionary<string, CustomFont>();
    private float xUnit = Utils.utils.xUnit;
    private float yUnit = Utils.utils.yUnit;
    private int fontScale = 4;
    private int fontRecyclePositionMax = 5;
    private float ageMultiplier = 0.1f;

    public AnimatedStore(Rect _rect, Texture2D _texture, Dictionary<string, CustomFont> _fonts) {
        this.defaultRect = _rect;
        this.addLabelRect = _rect;
        this.subtractLabelRect = _rect;
        this.subtractLabelRect.y += yUnit * fontRecyclePositionMax;
        this.texture = _texture;
        SetFonts();
        this.aliveTime = 0;
        this.drawMe = false;
    }

    private void SetFonts() {
        this.fonts["good"] = new CustomFont(Color.black, fontScale);
        this.fonts["bad"] = new CustomFont(Color.red, fontScale);
    }

    public void Manage() {
        UpdateScale();
        Draw();
        Age();
        Recycle();
    }

    private void Draw() {
        if (drawMe) {
            GUI.Label(
                GetLabelRect(),
                GetText(),
                GetFont()
            );
        }
    }

    public void Update(float x, float y) {
        defaultRect.x = x;
        defaultRect.y = y;
        addLabelRect.x = x;
        addLabelRect.y = y;
        subtractLabelRect.x = x;
        subtractLabelRect.y = y;
        this.fonts["good"].Update();
        this.fonts["bad"].Update();
    }

    private void UpdateScale() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        SetFonts();
    }

    private Rect GetLabelRect() {
        return this.quantity > 0 ? this.addLabelRect : this.subtractLabelRect;
    }

    private string GetText() {
        return (this.quantity > 0 ? "+" : "-") + Mathf.Abs(this.quantity).ToString();
    }

    private GUIStyle GetFont() {
        return this.quantity > 0 ? this.fonts["good"].font : this.fonts["bad"].font;
    }

    private void Age() {
        if (this.drawMe) {
            // adjust position of label
            this.addLabelRect.y -= yUnit * ageMultiplier * Mathf.Sign(this.quantity);
            this.subtractLabelRect.y -= yUnit * ageMultiplier * Mathf.Sign(this.quantity);
        }
    }

    private void Recycle() {
        if (this.drawMe && (Time.time - this.aliveTime) > this.maxAliveTime) {
            this.drawMe = false;
            this.aliveTime = 0;
            this.quantity = 0;
            this.addLabelRect.y = this.defaultRect.y;
            this.subtractLabelRect.y = this.defaultRect.y + (yUnit * fontRecyclePositionMax);
        }
    }

    public void Activate(int _quantity) {
        this.quantity = _quantity;
        this.aliveTime = Time.time;
        this.drawMe = true;
    }
}
