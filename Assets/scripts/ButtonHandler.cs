using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ButtonHandler {
    // contains starting position of buttons
    public static Dictionary<string, Button> buttons = new Dictionary<string, Button>();
    private static Button confirmReset;
    private static Dictionary<string, int> posParams = new Dictionary<string, int>();
    private static float xUnit;
    private static float yUnit;
    private static ButtonActions buttonActions = new ButtonActions();
    private static Dictionary<string, Texture2D> buttonTextures;
    public static bool reloadClicked;
    static int offset = 0;
    static int widthMultiplier = 5;
    static int heightMultiplier = 8;

    static ButtonHandler() {
        SetDefaults();
    }

    private static void SetDefaults() {
        SetButtonPositions();
        GenerateButtons();
        SetButtonPositions();
        UpdateButtons();
    }

    private static void SetButtonPositions() {
        // adjust from right
        posParams["buttonX"] = (int)(Screen.width - (xUnit * 7.5f));
        // adjust from top
        posParams["buttonY"] = (int)(yUnit * 5);
    }

    private static void GenerateButtons() {
        buttons["pause"] = new Button(
            "pause", offset, buttonActions, "main"
        );
        buttons["help"] = new Button(
            "help", offset + 1, buttonActions, "main"
        );
        buttons["settings"] = new Button(
            "settings", offset + 2, buttonActions, "main"
        );
        buttons["blog"] = new Button(
            "blog", offset, buttonActions, "settings"
        );
        buttons["gitlab"] = new Button(
            "gitlab", offset + 1, buttonActions, "settings"
        );
        buttons["back"] = new Button(
            "back", offset + 2, buttonActions, "settings"
        );
        buttons["reload"] = new Button(
            "reload", offset + 3, buttonActions, "settings"
        );

        confirmReset = new Button("confirmReset", offset, buttonActions, "settings");
    }

    public static void Manage() {
        bool updated = UpdatePositionValues();
        if (updated) {
            SetButtonPositions();
            UpdateButtons();
        }
    }

    public static bool UpdatePositionValues() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return false;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        return true;
    }

    public static void UpdateButtons() {
        foreach(KeyValuePair<string, Button> entry in buttons) {
            entry.Value.Update(posParams["buttonX"], posParams["buttonY"], xUnit * widthMultiplier, yUnit * heightMultiplier);
        }
        UpdateResetButton();
    }

    private static void UpdateResetButton() {
        confirmReset.Update(
            0,
            0,
            xUnit * 30,
            yUnit * 24
        );
        // need to update this after setting width/height to avoid cascading positioning recalculations
        confirmReset.rect.x = (int)(Screen.width / 2 - confirmReset.rect.width / 2);
        confirmReset.rect.y = (int)(Screen.height / 2 - confirmReset.rect.height / 2);
    }

    public static void Draw() {
        // confirmReset button is only visible once reload is clicked or game is over
        if (reloadClicked || State.state.gameOver) {
            if (GUI.Button(confirmReset.rect, confirmReset.texture)) {
                reloadClicked = false;
                confirmReset.Action();
                return;
            }
        }
        foreach (KeyValuePair<string, Button> entry in buttons) {
            if (entry.Value.screen != State.state.screen) {
               continue;
            }
            // draws button and checks for click; frame timing could explain why reset is hard to click
            if (GUI.Button(entry.Value.rect, entry.Value.texture)) {
                entry.Value.Action();
            }
        }
        State.state.HandleStates();
    }
}
